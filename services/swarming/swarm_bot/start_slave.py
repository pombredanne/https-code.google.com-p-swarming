#!/usr/bin/env python
# Copyright 2013 The Swarming Authors. All rights reserved.
# Use of this source code is governed by the Apache v2.0 license that can be
# found in the LICENSE file.

"""Start Slave.

This is a place holder script to start the slave. This script should be updated
by each project to pass the correct parameters to the slave.

This script is called to restart the slave if the slave version is updated
by the server.
"""



import logging

logging.error('start_slave.py doesn\'t know what machine configurations to '
              'use, not restarting slave.')
